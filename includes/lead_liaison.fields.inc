<?php

/**
 * @file
 * Webform Remote Post URL targets administration pages.
 */

/**
 * An overview of the URL repost targets associated with a given form.
 */
function lead_liaison_settings_form($form, &$form_state, $webform_node) {
  $pre = variable_get('lead_liaison_webform_'.$webform_node->nid,array());
  $form['#tree'] = TRUE;
  $form['#node'] = $webform_node;
  
  if (variable_get('lead_liaison_autoformtracking', '') === 'false'){
    $message = t('This will not work because you have "Automatic Form Tracking" disabled in the LL settings.  You can change that setting !here', array('!here' => l('here', 'admin/config/system/leadliaison')));
    drupal_set_message($message, 'warning');
  }

  // Internal to the form and never displayed to the screen.
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $webform_node->nid,
  );

  $targets = array('none'=>'Not Set (ignored)');

  foreach ($webform_node->webform['components'] as $cid => $component){
    $targets['Webform Components'][$component['form_key']] = $component['name'];
  }
  $form['ll']['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Prefill'),
  );
  $form['ll']['settings']['prefill'] = array(
    '#type' => 'select',
    '#title' => t('Prefill Fields?'),
    '#description' => t('Select "Yes" if you want the email, firstname, lastname, fullname to be pre-filled with info from Lead Liaison.'),
    '#options' => array('no' => 'No', 'yes' => 'Yes'),
    '#default_value' => (isset($pre['prefill']) ? $pre['prefill'] : ''), 
    '#required' => FALSE,
  );  
  $form['ll']['settings']['postprocess'] = array(
    '#type' => 'select',
    '#title' => t('Send to Lead Liaison?'),
    '#description' => t('This will send info to Lead Liaison (via rules) and append prospect identifier from lead liaison, !wikisection.', array('!wikisection' => l('see this wiki section.', 'http://wiki.leadliaison.com/display/LL/Web+Forms#WebForms-PostProcessingBeforeSubmittingtoLeadLiaison'))),
    '#options' => array('no' => 'No', 'yes' => 'Yes'),
    '#default_value' => (isset($pre['postprocess']) ? $pre['postprocess'] : ''), 
    '#required' => FALSE,
  );  

  $form['ll']['settings']['llwebformid'] = array(
    '#type' => 'textfield',
    '#title' => t('Lead Liaison webform ID?'),
    '#description' => t('The ID of the form.  This is found at the end of the form action url after "ll_existing_form_id=".'),
    '#default_value' => (isset($pre['llwebformid']) ? $pre['llwebformid'] : ''), 
    '#required' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="ll[settings][postprocess]"]' => array('value' => 'yes'),
      ),
    ),
  );  

  $form['ll']['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
  );
  $form['ll']['fields']['email'] = array(
    '#type' => 'select',
    '#title' => t('Email'),
    '#description' => t('Select the webform component that you want to be auto-tracked in Lead Liaison as "Email"'),
    '#options' => $targets,
    '#default_value' => (isset($pre['email']['form_key']) ? $pre['email']['form_key'] : ''), 
    '#required' => FALSE,
  );


  $form['ll']['fields']['fullname'] = array(
    '#type' => 'select',
    '#title' => t('Full Name'),
    '#description' => t('Select the webform component that you want to be auto-tracked in Lead Liaison as "name"'),
    '#options' => $targets,
    '#default_value' => (isset($pre['fullname']['form_key']) ? $pre['fullname']['form_key'] : ''), 
    '#required' => FALSE,
  );


  $form['ll']['fields']['firstname'] = array(
    '#type' => 'select',
    '#title' => t('First Name'),
    '#description' => t('Select the webform component that you want to be auto-tracked in Lead Liaison as "first name"'),
    '#options' => $targets,
    '#default_value' => (isset($pre['firstname']['form_key']) ? $pre['firstname']['form_key'] : ''), 
    '#required' => FALSE,
  );

  $form['ll']['fields']['lastname'] = array(
    '#type' => 'select',
    '#title' => t('Last Name'),
    '#description' => t('Select the webform component that you want to be auto-tracked in Lead Liaison as "last name"'),
    '#options' => $targets,
    '#default_value' => (isset($pre['lastname']['form_key']) ? $pre['lastname']['form_key'] : ''), 
    '#required' => FALSE,
  );


  $form['ll']['fields']['company'] = array(
    '#type' => 'select',
    '#title' => t('Company'),
    '#description' => t('Select the webform component that you want to be auto-tracked in Lead Liaison as "company"'),
    '#options' => $targets,
    '#default_value' => (isset($pre['company']['form_key']) ? $pre['company']['form_key'] : ''), 
    '#required' => FALSE,
  );  
  
  $form['save_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 45,
  );

  $form['#validate'] = array('lead_liaison_settings_form_validate');

  return $form;
}

/**
 * Validate handler for lead_liaison_settings_form().
 */
function lead_liaison_settings_form_validate($form, &$form_state) {
  //check to make sure fields aren't used twice.
  foreach ($form_state['values']['ll']['fields'] as $ll => $val) {
    if($val == 'none'){
      unset($form_state['values']['ll']['fields'][$ll]);
    }
    else{
      $check[] = $val;
    }
  }
  $valuenum = count($check);
  $uniquenum = count(array_unique($check));

  if ($valuenum != $uniquenum){
    form_set_error('', t('You can not use the same webform component twice.  Please make sure each of the fields below has a unique component in it.'));
  }
  if ($form_state['values']['ll']['settings']['postprocess'] == "yes" && empty($form_state['values']['ll']['settings']['llwebformid'])){
     form_set_error('', t('Webform ID is required if sending to Lead Liaison.'));
  }
}

/**
 * Submit handler for lead_liaison_settings_form().
 */
function lead_liaison_settings_form_submit($form, &$form_state) {
  $node = node_load($form_state['values']['nid']);

  $tree =_webform_components_tree_build($node->webform['components'], $form_state['webform']['component_tree'], 0, $form_state['webform']['page_count']);
  foreach($form_state['values']['ll']['fields'] as $llfield => $form_key){
    $path = _lead_liaison_array_find_deep($tree, $form_key);
    $store[$llfield]['form_key'] = $form_key;
    $store[$llfield]['id'] = "edit-submitted";
    foreach ($path as $item){
      if(is_integer($item)){
        $store[$llfield]['id'] .= '-' . str_replace('_', '-', $node->webform['components'][$item]['form_key']);
      }
    }
    //$llfieldid[$llfield]['id'] .= '-' . str_replace('_', '-', $settings[$llfield]);
  }
  $store['prefill'] = $form_state['values']['ll']['settings']['prefill'];
  $store['postprocess'] = $form_state['values']['ll']['settings']['postprocess'];
  $store['llwebformid'] = $form_state['values']['ll']['settings']['llwebformid'];
  
  drupal_set_message('It is recommended that you flush your cache after submitting this form.', 'warning');
  variable_set('lead_liaison_webform_'.$form_state['values']['nid'], $store);
}



/**
 * Multidimentional array_search
 */
function _lead_liaison_array_find_deep($array, $search, $keys = array()) {
  foreach($array as $key => $value) {
    if (is_array($value)) {
      $sub = _lead_liaison_array_find_deep($value, $search, array_merge($keys, array($key)));
    if (count($sub)) {
      return $sub;
    }
    } elseif ($value === $search) {
      return array_merge($keys, array($key));
    }
  }
  
  return array();
}
