
-- SUMMARY --

This module was created to help integrate a drupal site with Lead Liaison.

-- REQUIREMENTS --

Webform module: http://drupal.org/project/webform

The module was developed against the 7.x-3.18 version fo Webform, but it should
work with other 3.x versions.

-- INSTALLATION --

* Install as usual, see
  http://drupal.org/documentation/install/modules-themes/modules-7 for further
  information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Administer Lead Liaison Settings
  - Set lead liaison auto-tracking fields for webform

* Customize the settings: admin/config/system/leadliaison

* Configure a webform to use: node/[your_webform_nodeid_here]/webform/leadliaison

-- FEATURES --

* Ability to easily add the LL tracking code to a drupal website through the web interface
* Visibility settings (roles, pages, users) for the tracking code.  Example: prevent the code from showing up if logged in as administrator role.
* SSL support.  It detects if the site is http or https and changes the tracking code as needed.
* Webform integration 
	- Select which webform fields are email/fullname/lastname/firstname/company for LL auto-tracking.  
	- select if you want these to pre-fill (un-tested).  
	- ability to set post processing flag on webforms.
	- All of this on a per webform basis.
* Permissions to control what roles have the ability to change LL settings and LL webform settings.
