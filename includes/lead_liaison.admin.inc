<?php

/**
 * @file
 * Administrative page callbacks for the lead_liaison module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function lead_liaison_admin_settings_form($form_state) {
  drupal_set_message('NOTE: You may need to flush your cache after submitting this form.', 'status');

  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['account']['lead_liaison_account'] = array(
    '#title' => t('LL Customer ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('lead_liaison_account', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('This ID can be found in your tracking code right after "llcustid=".'),
  );
  

  // Visibility settings.
  $form['tracking_title'] = array(
    '#type' => 'item',
    '#title' => t('Tracking scope'),
  );
  $form['tracking'] = array(
    '#type' => 'vertical_tabs',
    // '#attached' => array(
      // 'js' => array(drupal_get_path('module', 'lead_liaison') . 'js/lead_liaison.admin.js'),
    // ),
  );



  $form['tracking']['autotracking'] = array(
    '#type' => 'fieldset',
    '#title' => t('Autotracking'),
  );

  $form['tracking']['autotracking'] ['lead_liaison_autoformtracking'] = array(
    '#type' => 'select',
    '#title' => t('Automatic Form Tracking'),
    '#description' => t('See !thisurl for an explaination.', array("!thisurl" => l('this url', 'http://wiki.leadliaison.com/display/LL/Automatic+Web+Form+Tracking#AutomaticWebFormTracking-TurningOffAutomaticWebFormTracking'))),
    '#options' => array('true'=>'Enabled (default)', 'false'=>'Disabled'),
    '#default_value' => variable_get('lead_liaison_autoformtracking', 'true'),
    '#required' => TRUE,
  );

  // Page specific visibility configurations.
  $php_access = user_access('use PHP for tracking visibility');
  $visibility = variable_get('lead_liaison_visibility_pages', 0);
  $pages = variable_get('lead_liaison_pages', lead_liaison_PAGES);

  $form['tracking']['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if ($visibility == 2 && !$php_access) {
    $form['tracking']['page_vis_settings'] = array();
    $form['tracking']['page_vis_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['tracking']['page_vis_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if (module_exists('php') && $php_access) {
      $options[] = t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['tracking']['page_vis_settings']['lead_liaison_visibility_pages'] = array(
      '#type' => 'radios',
      '#title' => t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['tracking']['page_vis_settings']['lead_liaison_pages'] = array(
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $pages,
      '#description' => $description,
      '#rows' => 10,
    );
  }

  // Render the role overview.
  $form['tracking']['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
  );

  $form['tracking']['role_vis_settings']['lead_liaison_visibility_roles'] = array(
    '#type' => 'radios',
    '#title' => t('Add tracking for specific roles'),
    '#options' => array(
      t('Add to the selected roles only'),
      t('Add to every role except the selected ones'),
    ),
    '#default_value' => variable_get('lead_liaison_visibility_roles', 0),
  );

  $role_options = array_map('check_plain', user_roles());
  $form['tracking']['role_vis_settings']['lead_liaison_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('lead_liaison_roles', array()),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
  );

  // Standard tracking configurations.
  $form['tracking']['user_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Users'),
  );
  $t_permission = array('%permission' => t('opt-in or out of tracking'));
  $form['tracking']['user_vis_settings']['lead_liaison_custom'] = array(
    '#type' => 'radios',
    '#title' => t('Allow users to customize tracking on their account page'),
    '#options' => array(
      t('No customization allowed'),
      t('Tracking on by default, users with %permission permission can opt out', $t_permission),
      t('Tracking off by default, users with %permission permission can opt in', $t_permission),
    ),
    '#default_value' => variable_get('lead_liaison_custom', 0),
  );

  // Privacy specific configurations.
  $form['tracking']['privacy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Privacy'),
  );

  $form['tracking']['privacy']['lead_liaison_privacy_donottrack'] = array(
    '#type' => 'checkbox',
    '#title' => t('Universal web tracking opt-out'),
    '#description' => t('If enabled and your server receives the <a href="@donottrack">Do-Not-Track</a> header from the client browser, the Lead Liaison module will not embed any tracking code into your site. Compliance with Do Not Track could be purely voluntary, enforced by industry self-regulation, or mandated by state or federal law. Please accept your visitors privacy. If they have opt-out from tracking and advertising, you should accept their personal decision. This feature is currently limited to logged in users and disabled page caching.', array('@donottrack' => 'http://donottrack.us/')),
    '#default_value' => variable_get('lead_liaison_privacy_donottrack', 1),
  );

  // Advanced feature configurations.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['lead_liaison_js_scope'] = array(
    '#type' => 'select',
    '#title' => t('JavaScript scope'),
    '#description' => t('Lead Liaison recommends adding the JavaScript files to the header for performance reasons.'),
    '#options' => array(
      'footer' => t('Footer'),
      'header' => t('Header'),
    ),
    '#default_value' => variable_get('lead_liaison_js_scope', 'header'),
  );

  $form['advanced']['codesnippet'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom JavaScript code'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('You can add custom Lead Liaison code snippets here (<a href="@snippets">examples</a>). These will be added every time tracking is in effect. Before you add your custom code, you should check with Lead Liaison to make sure this is appropriate for your use.  <strong>Do not include the &lt;script&gt; tags</strong>, and always end your code with a semicolon (;).', array('@snippets' => 'http://wiki.leadliaison.com/display/LL/Automatic+Web+Form+Tracking#AutomaticWebFormTracking-Identifyformfieldsbyidattribute:')),
  );
  $form['advanced']['codesnippet']['lead_liaison_codesnippet_before'] = array(
    '#type' => 'textarea',
    '#title' => t('Code snippet (before)'),
    '#default_value' => variable_get('lead_liaison_codesnippet_before', ''),
    '#rows' => 5,
    '#description' => t("Code in this textarea will be added <strong>before</strong> http://cdn.leadliaison.com/scripts/lazyload-min.js (http) or https://app.leadliaison.com/tracking_engine/collector.php (https)"),
  );
  $form['advanced']['codesnippet']['lead_liaison_codesnippet_after'] = array(
    '#type' => 'textarea',
    '#title' => t('Code snippet (after)'),
    '#default_value' => variable_get('lead_liaison_codesnippet_after', ''),
    '#rows' => 5,
    '#description' => t("Code in this textarea will be added <strong>after</strong> http://cdn.leadliaison.com/scripts/lazyload-min.js (http) or https://app.leadliaison.com/tracking_engine/collector.php (https)"),
  );

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function lead_liaison_admin_settings_form_validate($form, &$form_state) {
  // Trim some text values.
  $form_state['values']['lead_liaison_account'] = trim($form_state['values']['lead_liaison_account']);
  $form_state['values']['lead_liaison_pages'] = trim($form_state['values']['lead_liaison_pages']);
  $form_state['values']['lead_liaison_codesnippet_before'] = trim($form_state['values']['lead_liaison_codesnippet_before']);
  $form_state['values']['lead_liaison_codesnippet_after'] = trim($form_state['values']['lead_liaison_codesnippet_after']);

  // This is for the Newbie's who cannot read a text area description.
  if (stristr($form_state['values']['lead_liaison_codesnippet_before'], 'tracking_engine/collector.php')) {
    form_set_error('lead_liaison_codesnippet_before', t('Do not add the tracker code provided by Lead Liaison into the javascript code snippets! This module already builds the tracker code based on your Google Analytics account number and settings.'));
  }
  if (stristr($form_state['values']['lead_liaison_codesnippet_after'], 'tracking_engine/collector.php')) {
    form_set_error('lead_liaison_codesnippet_after', t('Do not add the tracker code provided by Lead Liaison into the javascript code snippets! This module already builds the tracker code based on your Google Analytics account number and settings.'));
  }
  if (preg_match('/(.*)<\/?script(.*)>(.*)/i', $form_state['values']['lead_liaison_codesnippet_before'])) {
    form_set_error('lead_liaison_codesnippet_before', t('Do not include the &lt;script&gt; tags in the javascript code snippets.'));
  }
  if (preg_match('/(.*)<\/?script(.*)>(.*)/i', $form_state['values']['lead_liaison_codesnippet_after'])) {
    form_set_error('lead_liaison_codesnippet_after', t('Do not include the &lt;script&gt; tags in the javascript code snippets.'));
  }
}
