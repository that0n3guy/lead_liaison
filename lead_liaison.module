<?php

/*
 * @file
 * Drupal Module: lead_liaison
 * Adds the required Javascript to the bottom of all your Drupal pages
 * to allow tracking by the lead liaison marketing automation package.
 * 
 * thanks: Many thanks to the googleanalytics module.  Much of the code in this module came from that module.
 *
 */


// Remove tracking from all administrative pages, see http://drupal.org/node/34970.
define('lead_liaison_PAGES', "admin\nadmin/*\nbatch\nnode/add*\nnode/*/*\nuser/*/*");
//define('lead_liaison_TRACKFILES_EXTENSIONS', '7z|aac|arc|arj|asf|asx|avi|bin|csv|doc|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip');

/**
 * Implements hook_permission().
 */
function lead_liaison_permission() {
  return array(
    'administer lead liaison' => array(
      'title' => t('Administer Lead Liaison Settings '),
      'description' => t('Allow users to change the lead liaison settings.'),
    ),
    'webform lead liaison' => array(
      'title' => t('Set lead liaison auto-tracking fields for webform'),
      'description' => t('Allow users to define which webform components should be auto-tracked in Lead Liaison.'),
    ),
  );
}


/**
 * Implements hook_menu().
 *
 * @see webform_menu_load()
 */
function lead_liaison_menu() {
  $items = array();

  // Targets list, %webform_menu is an auto-loader wildcard component
  // provided by the webform module (method is webform_menu_load), and it
  // auto-loads a webform node.
  $items['node/%webform_menu/webform/leadliaison'] = array(
    'title' => 'Lead Liaison Fields',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lead_liaison_settings_form', 1),
    'access callback' => 'user_access',
    'access arguments' => array('webform lead liaison'),
    'file' => 'includes/lead_liaison.fields.inc',
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/system/leadliaison'] = array(
    'title' => 'Lead Liaison',
    'description' => 'Configure Lead Liaison tracker settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lead_liaison_admin_settings_form'),
    'access arguments' => array('administer lead liaison'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/lead_liaison.admin.inc',
  );

  return $items;
}

// 
// function lead_liaison_form_alter(&$form, &$form_state, $form_id){
  // if (strpos($form_id, 'webform_client_form_') === 0) {
//     
  // }
// }

/**
 * Implements hook_page_alter() to insert JavaScript to the appropriate scope/region of the page.
 */
function lead_liaison_page_alter(&$page) {
  global $user;

  $id = variable_get('lead_liaison_account', '');

  // Get page status code for visibility filtering.
  $status = drupal_get_http_header('Status');
  $trackable_status_codes = array(
    '403 Forbidden',
    '404 Not Found',
  );

  // 1. Check if the LL account number has a value.
  // 2. Track page views based on visibility value.
  // 3. Check if we should track the currently active user's role.
  // 4. Ignore pages visibility filter for 404 or 403 status codes.
  if (!empty($id) && (_lead_liaison_visibility_pages() || in_array($status, $trackable_status_codes)) && _lead_liaison_visibility_user($user)) {
    
    //get the per-webform info
    if (arg(0) == 'node' && arg(1) != 'add' && arg(2) != 'edit'){
      $llwebform = variable_get('lead_liaison_webform_' . arg(1),array());
      //build an array of llfield => field id
      foreach($llwebform as $name => $val){
        //only do the fields, setttings are not in an array.
        if(is_array($val)){
          $llfields[$name] = $val['id'];
        }
      }
    }
    
    // We allow different scopes. Default to 'header' but allow user to override if they really need to.
    $scope = variable_get('lead_liaison_js_scope', 'header');

    // Track access denied (403) and file not found (404) pages.
    if ($status == '403 Forbidden') {
      // See http://www.google.com/support/analytics/bin/answer.py?answer=86927
      $url_custom = '"/403.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer';
    }
    elseif ($status == '404 Not Found') {
      $url_custom = '"/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer';
    }

    // Add any custom code snippets if specified.
    $codesnippet_before = variable_get('lead_liaison_codesnippet_before', '');
    $codesnippet_after = variable_get('lead_liaison_codesnippet_after', '');
    
    // Build tracker code.
    $script = 'var llcustid = ' . $id . '; var ll_use_automatic_form_tracking = ' . variable_get('lead_liaison_autoformtracking', 'true') . ';';
    if(isset($llwebform['prefill']) && $llwebform['prefill'] === 'yes'){
      $script .= ' var ll_prefill_automatic_tracked_form_fields = true;';
    }
    if(isset($llwebform['postprocess']) && $llwebform['postprocess'] === 'yes'){
      $script .= ' var ll_append_identifier_to_form = true;';
    }
    
    $script .= "var ll_use_lazyload = true; LazyLoad.js('https://app.leadliaison.com/tracking_engine/collector.php');";

    if (!empty($codesnippet_before)) {
      $script .= $codesnippet_before;
    }
    
    // different tracking code for https than http
    $secure = false;
    if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off'){
      $secure = true;
    }   
    
    if (!empty($codesnippet_after)) {
      $script .= $codesnippet_after;
    }
    if (!empty($llfields)){
      $script .= ' var ll_fields_map_settings_by_field_id_identifier =' . json_encode($llfields) . ';';
    }
    
    //secure sites load in a different order
    if($secure){
      drupal_add_js('https://d2tkczi6ecqjoh.cloudfront.net/scripts/lazyload-min.js', array('scope' => $scope, 'type' => 'external'));
      drupal_add_js($script, array('scope' => $scope, 'type' => 'inline'));
    }
    else{
      drupal_add_js('http://cdn.leadliaison.com/scripts/lazyload-min.js', array('scope' => $scope, 'type' => 'external'));
      drupal_add_js($script, array('scope' => $scope, 'type' => 'inline'));
    }
  }
}



/**
 * Implements hook_field_extra_fields().
 */
function lead_liaison_field_extra_fields() {
  $extra['user']['user']['form']['lead_liaison'] = array(
    'label' => t('Lead Liaison configuration'),
    'description' => t('Lead Liaison module form element.'),
    'weight' => 3,
  );

  return $extra;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allow users to decide if tracking code will be added to pages or not.
 */
function lead_liaison_form_user_profile_form_alter(&$form, &$form_state) {
  $account = $form['#user'];
  $category = $form['#user_category'];

  if ($category == 'account' && user_access('opt-in or out of tracking') && ($custom = variable_get('lead_liaison_custom', 0)) != 0 && _lead_liaison_visibility_roles($account)) {
    $form['lead_liaison'] = array(
      '#type' => 'fieldset',
      '#title' => t('Lead Liaison configuration'),
      '#weight' => 3,
      '#collapsible' => TRUE,
      '#tree' => TRUE
    );

    switch ($custom) {
      case 1:
        $description = t('Users are tracked by default, but you are able to opt out.');
        break;

      case 2:
        $description = t('Users are <em>not</em> tracked by default, but you are able to opt in.');
        break;
    }

    // Disable tracking for visitors who have opted out from tracking via DNT (Do-Not-Track) header.
    $disabled = FALSE;
    if (variable_get('lead_liaison_privacy_donottrack', 1) && !empty($_SERVER['HTTP_DNT'])) {
      $disabled = TRUE;

      // Override settings value.
      $account->data['lead_liaison']['custom'] = FALSE;

      $description .= '<span class="admin-disabled">';
      $description .= ' ' . t('You have opted out from tracking via browser privacy settings.');
      $description .= '</span>';
    }

    $form['lead_liaison']['custom'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable user tracking'),
      '#description' => $description,
      '#default_value' => isset($account->data['lead_liaison']['custom']) ? $account->data['lead_liaison']['custom'] : ($custom == 1),
      '#disabled' => $disabled,
    );

    return $form;
  }
}

/**
 * Implements hook_user_presave().
 */
function lead_liaison_user_presave(&$edit, $account, $category) {
  if (isset($edit['lead_liaison']['custom'])) {
    $edit['data']['lead_liaison']['custom'] = $edit['lead_liaison']['custom'];
  }
}



/**
 * Based on visibility setting this function returns TRUE if LL code should
 * be added for the current role and otherwise FALSE.
 */
function _lead_liaison_visibility_roles($account) {

  $visibility = variable_get('lead_liaison_visibility_roles', 0);
  $enabled = $visibility;
  $roles = variable_get('lead_liaison_roles', array());

  if (array_sum($roles) > 0) {
    // One or more roles are selected.
    foreach (array_keys($account->roles) as $rid) {
      // Is the current user a member of one of these roles?
      if (isset($roles[$rid]) && $rid == $roles[$rid]) {
        // Current user is a member of a role that should be tracked/excluded from tracking.
        $enabled = !$visibility;
        break;
      }
    }
  }
  else {
    // No role is selected for tracking, therefore all roles should be tracked.
    $enabled = TRUE;
  }

  return $enabled;
}

/**
 * Tracking visibility check for an user object.
 *
 * @param $account
 *   A user object containing an array of roles to check.
 * @return boolean
 *   A decision on if the current user is being tracked by lead liaison.
 */
function _lead_liaison_visibility_user($account) {

  $enabled = FALSE;

  // Is current user a member of a role that should be tracked?
  if (_lead_liaison_visibility_header($account) && _lead_liaison_visibility_roles($account)) {

    // Use the user's block visibility setting, if necessary.
    if (($custom = variable_get('lead_liaison_custom', 0)) != 0) {
      if ($account->uid && isset($account->data['lead_liaison']['custom'])) {
        $enabled = $account->data['lead_liaison']['custom'];
      }
      else {
        $enabled = ($custom == 1);
      }
    }
    else {
      $enabled = TRUE;
    }

  }

  return $enabled;
}


/**
 * Based on visibility setting this function returns TRUE if GA code should
 * be added to the current page and otherwise FALSE.
 */
function _lead_liaison_visibility_pages() {
  static $page_match;

  // Cache visibility result if function is called more than once.
  if (!isset($page_match)) {

    $visibility = variable_get('lead_liaison_visibility_pages', 0);
    $setting_pages = variable_get('lead_liaison_pages', lead_liaison_PAGES);

    // Match path if necessary.
    if (!empty($setting_pages)) {
      // Convert path to lowercase. This allows comparison of the same path
      // with different case. Ex: /Page, /page, /PAGE.
      $pages = drupal_strtolower($setting_pages);
      if ($visibility < 2) {
        // Convert the Drupal path to lowercase
        $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
        // Compare the lowercase internal and lowercase path alias (if any).
        $page_match = drupal_match_path($path, $pages);
        if ($path != $_GET['q']) {
          $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
        }
        // When $visibility has a value of 0, the tracking code is displayed on
        // all pages except those listed in $pages. When set to 1, it
        // is displayed only on those pages listed in $pages.
        $page_match = !($visibility xor $page_match);
      }
      elseif (module_exists('php')) {
        $page_match = php_eval($setting_pages);
      }
      else {
        $page_match = FALSE;
      }
    }
    else {
      $page_match = TRUE;
    }

  }
  return $page_match;
}

/**
 * Based on headers send by clients this function returns TRUE if GA code should
 * be added to the current page and otherwise FALSE.
 */
function _lead_liaison_visibility_header($account) {

  if (($account->uid || variable_get('cache', 0) == 0) && variable_get('lead_liaison_privacy_donottrack', 1) && !empty($_SERVER['HTTP_DNT'])) {
    // Disable tracking if caching is disabled or a visitors is logged in and
    // have opted out from tracking via DNT (Do-Not-Track) header.
    return FALSE;
  }

  return TRUE;
}


/**
 * Implements hook_rules_action_info().
 *
 */
function lead_liaison_rules_action_info() {
  $actions = array(
    'lead_liaison_post_webform_submission' => array(
      'label' => t('Send webform submission to Lead Liaison.'),
      'base' => 'lead_liaison_post_webform_submission',
      'group' => t('Lead Liaison'),
      'access callback' => 'rules_node_admin_access',
      'parameter' => array(
        'node' => array(
          'type' => 'integer',
          'label' => t('Webform Node id'),
          'description' => t('The webform node id.'),
        ),
        'sid' => array(
          'type' => 'text',
          'label' => t('Submission ID'),
          'description' => t('The ID of a webform submission. If omitted all submissions of the specified node ID will be fetched.'),
          'default value' => '[data:sid]',
        ),
      ),
    ),
    
  );
  return $actions;
}


/**
 * Implements hook_rules_condition_info().
 */
function lead_liaison_rules_condition_info() {
  return array(
    'lead_liaison_load_webforms_check' => array(
      'label' => t('Check if webform should be submitted to Lead Liaison.'),
      'arguments' => array(
        'nid' => array('type' => 'integer', 'label' => t('The ID of the node you want to check')),
      ),
      'group' => t('Lead Liaison'),
    ),
  );
}


/**
 * Condition lead_liaison_load_webforms_check.
 *
 */
function lead_liaison_load_webforms_check($nid) {
  $variable = variable_get($variable_name);
  $pre = variable_get('lead_liaison_webform_'.$nid, array());
  
  if (isset($pre['postprocess']) && $pre['postprocess'] == 'yes') {
    return true;
  }
  return false;
}



/**
 * Rules action callback to email submission.
 */
function lead_liaison_post_webform_submission($nid, $sid) {
  //for future support of json if needed. @todo
  $target['type'] = 'post';
  
  //grab the LL webform ID form the webform lead liaison settings.
  $pre = variable_get('lead_liaison_webform_'.$nid,array());
  $llformid = $pre['llwebformid'];
  
  $node = node_load($nid);
  // Make sure the needed functions are available.
  module_load_include('inc', 'webform', 'includes/webform.submissions');
    
  $component_tree = array();
  $page_count = 1;
  $submission = webform_get_submission($nid, $sid, TRUE);
 
  // Remove excluded components.
  $components = $node->webform['components'];

  //get the components+values in a form that is somewhat similar to an actual form post for this webform.
  _lead_liaison_tree_build($components, $component_tree, 0, $page_count, $submission);
  
  //get the lead liaison account ID, set in LL module settings
  $id = variable_get('lead_liaison_account', '');
  
  if(!empty($id)){
    //$target['url'] = "http://requestb.in/r82yw8r8?ll_existing_form_id=$llformid&ll_cust_id=$id";
    $target['url'] = "http://app.leadliaison.com/process-existing-webform.php?ll_existing_form_id=$llformid&ll_cust_id=$id";
    
    // Acceptable server response codes.
    $benign_reponse_codes = array('200', '301', '302', '307');
  
    // Post data to target. @todo
    if($target['type'] == 'json') {
      // JSON Encode the payload.
      $post_data = json_encode($component_tree);
      
      // Repost data to each target. Begin by setting the
      // options for drupal_http_request().
      $drupal_http_request_options = array(
        'method' => 'POST',
        'data' => $post_data,
        'timeout' => 15,
        'headers' => array('Content-Type' => 'application/json', 'Accept' => '*/*'),
      );
    }
    else {
      // URL-encode the payload. use '', '&' to remove the extra &amp; that will go infront sometimes. 
      $post_data = drupal_http_build_query($component_tree);
      // Repost data to each target. Begin by setting the
      // options for drupal_http_request().
      $drupal_http_request_options = array(
        'method' => 'POST',
        'data' => $post_data,
        'timeout' => 15,
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      );
    }
  
    // Where the magic happens:
    $request = drupal_http_request($target['url'], $drupal_http_request_options);
    //$request = drupal_http_request('http://requestb.in/t215adt2', $drupal_http_request_options);
    
    // Log any errors.
    if (isset($request->code) && !in_array($request->code, $benign_reponse_codes)) {
      $log_msg = 'A remote (%type) post to %url by webform node ID %id returned a ';
      $log_msg .= '\'%code\' code, which is a different HTTP response code ';
      $log_msg .= 'than expected. Please make sure that the remote post URL ';
      $log_msg .= 'is correct in the Remote Posts webform settings, and that ';
      $log_msg .= 'the post was received in the remote system.';
      watchdog(
        'webform_remote_post',
        $log_msg,
        array(
          '%id'   => $node->nid,
          '%url'  => $target['url'],
          '%code' => $request->code,
          '%type' => $target['type'],
        ),
        WATCHDOG_WARNING);
    }
  }
}


function _lead_liaison_tree_build($src, &$tree, $parent, &$page_count, $submission) {
  foreach ($src as $cid => $component) {
    if ($component['pid'] == $parent) {
      _lead_liaison_tree_build($src, $component, $cid, $page_count, $submission);
      if ($component['type'] == 'pagebreak') {
        $page_count++;
      }
      $name = $src[$cid]['form_key'];
      if($parent == 0){
        if ($component['type'] == 'fieldset') {
          //if its a fieldset, just make it an empty array that we can add crap into.
          $tree['submitted'][$name] = $component;
        }
        else{
          if(count($submission->data[$cid]) > 1){
            //for multi-select support, not sure if this works on LL though
            $tree['submitted'][$name] = $submission->data[$cid]['value'];
          }
          elseif(!empty($submission->data[$cid]['value'][0])){
            $tree['submitted'][$name] = $submission->data[$cid]['value'][0];
          }
        }
      }
      else{
        $parentname = $src[$parent]['form_key'];
        if ($component['type'] == 'fieldset') {
          //if its a fieldset, just make it an empty array that we can add crap into.
          $tree[$name] = $component;
        }
        else{
          if(count($submission->data[$cid]) > 1){
            //for multi-select support, not sure if this works on LL though
            $tree[$name] = $submission->data[$cid]['value'];
          }
          elseif(!empty($submission->data[$cid]['value'][0])){
            $tree[$name] = $submission->data[$cid]['value'][0];
          }
        }
      }
    }
  }
  return $tree;
}
